package com.psixgame.apps.squarecamera;

/**
 * Created by ZOG on 12/20/2017.
 */

public final class VideoProperties {

    private int mWidth;
    private int mHeight;
    private int mRotation;

    public VideoProperties() {
    }

    public VideoProperties(final int _width, final int _height, final int _rotation) {
        mWidth = _width;
        mHeight = _height;
        mRotation = _rotation;
    }

    public int getWidth() {
        return mWidth;
    }

    public VideoProperties setWidth(final int _width) {
        mWidth = _width;
        return this;
    }

    public int getHeight() {
        return mHeight;
    }

    public VideoProperties setHeight(final int _height) {
        mHeight = _height;
        return this;
    }

    public int getRotation() {
        return mRotation;
    }

    public VideoProperties setRotation(final int _rotation) {
        mRotation = _rotation;
        return this;
    }
}

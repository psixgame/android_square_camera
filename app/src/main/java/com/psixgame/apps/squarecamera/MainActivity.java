package com.psixgame.apps.squarecamera;

import android.Manifest;
import android.media.MediaMetadataRetriever;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.widget.Button;
import android.widget.TextView;

import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.FFmpegLoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;
import com.wonderkiln.camerakit.CameraKit;
import com.wonderkiln.camerakit.CameraKitEventListenerAdapter;
import com.wonderkiln.camerakit.CameraKitVideo;
import com.wonderkiln.camerakit.CameraView;
import com.wonderkiln.camerakit.Size;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hugo.weaving.DebugLog;

public class MainActivity extends BaseActivity {

    private static final int PERMISSION_CODE_CAMERA = 1000;
    private static final int PERMISSION_CODE_RECORD_AUDIO = 1001;
    private static final int PERMISSION_CODE_WRITE_EXTERNAL_STORAGE = 1002;

    @BindView(R.id.cvCamera_AM)
    CameraView cvCamera;
    @BindView(R.id.tvStatus_AM)
    TextView tvStatus;
    @BindView(R.id.btnStart_AM)
    Button btnStart;
    @BindView(R.id.btnStop_AM)
    Button btnStop;

    private FFmpeg ffmpeg;

    @DebugLog
    @Override
    protected void onCreate(Bundle _savedInstanceState) {
        super.onCreate(_savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        ffmpeg = FFmpeg.getInstance(getApplicationContext());
        loadLibrary();
        tvStatus.setMovementMethod(new ScrollingMovementMethod());
        cvCamera.addCameraKitListener(mListener);
        cvCamera.setVideoQuality(CameraKit.Constants.VIDEO_QUALITY_720P);
    }

    @DebugLog
    @Override
    protected void onStart() {
        super.onStart();
        checkPermissionsAndProceedToCamera();
    }

    @DebugLog
    @Override
    protected void onStop() {
        super.onStop();
        cvCamera.stop();
    }

    @DebugLog
    @OnClick(R.id.btnStart_AM)
    void onClickStart() {
        tvStatus.setText("");
        cvCamera.captureVideo();
    }

    @DebugLog
    @OnClick(R.id.btnStop_AM)
    void onClickStop() {
        cvCamera.stopVideo();
    }

    @DebugLog
    private void checkPermissionsAndProceedToCamera() {
        if (!mPermissionHelper.hasPermission(Manifest.permission.CAMERA)) {
            mPermissionHelper.requestPermission(this, Manifest.permission.CAMERA, PERMISSION_CODE_CAMERA, this);
        } else if (!mPermissionHelper.hasPermission(Manifest.permission.RECORD_AUDIO)) {
            mPermissionHelper.requestPermission(this, Manifest.permission.RECORD_AUDIO, PERMISSION_CODE_RECORD_AUDIO, this);
        } else if (!mPermissionHelper.hasPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            mPermissionHelper.requestPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE, PERMISSION_CODE_WRITE_EXTERNAL_STORAGE, this);
        } else {
            enableCameraControls();
            cvCamera.start();
        }
    }

    @DebugLog
    @Override
    public final void onPermissionResult(final String _permission, final int _code, final boolean _granted) {
        super.onPermissionResult(_permission, _code, _granted);
        if (_granted) checkPermissionsAndProceedToCamera();
    }

    private void enableCameraControls() {
        btnStart.setEnabled(true);
        btnStop.setEnabled(true);
    }

    private final CameraKitEventListenerAdapter mListener = new CameraKitEventListenerAdapter() {
        @DebugLog
        @Override
        public final void onVideo(final CameraKitVideo _video) {
            updateStatus("Video recorded");
            cropVideo(_video.getVideoFile());
        }
    };

    private void loadLibrary() {
        try {
            ffmpeg.loadBinary(new FFmpegLoadBinaryResponseHandler() {
                @DebugLog
                @Override
                public void onFailure() {

                }

                @DebugLog
                @Override
                public void onSuccess() {

                }

                @DebugLog
                @Override
                public void onStart() {

                }

                @DebugLog
                @Override
                public void onFinish() {

                }
            });
        } catch (final FFmpegNotSupportedException _e) {
            _e.printStackTrace();
            showDialog("ffmpeg error: " + _e.toString());
        }
    }

    @DebugLog
    private void executeFfmpeg() {
        final String[] cmd = new String[]{ "-version" };
        try {
            ffmpeg.execute(cmd, new ExecuteBinaryResponseHandler() {
                @DebugLog
                @Override
                public void onSuccess(final String message) {
                    updateStatus("Success executing " + cmd + "\n" + message);
                }

                @DebugLog
                @Override
                public void onProgress(final String message) {
                    updateStatus("Progress executing " + cmd + "\n" + message);
                }

                @DebugLog
                @Override
                public void onFailure(final String message) {
                    updateStatus("Failure executing " + cmd + "\n" + message);
                }

                @DebugLog
                @Override
                public void onStart() {
                    updateStatus("Started executing " + cmd);
                }

                @DebugLog
                @Override
                public void onFinish() {
                    updateStatus("Finished executing " + cmd);
                }
            });
        } catch (final FFmpegCommandAlreadyRunningException _e) {
            _e.printStackTrace();
            showDialog("ffmpeg error: " + _e.toString());
        }
    }

    @DebugLog
    private void updateStatus(final String _status) {
        tvStatus.append("\n");
        tvStatus.append(_status);
    }

    @DebugLog
    private void cropVideo(final File _file) {
        updateStatus("Cropping video");
        final File croppedFile = prepareVideoFile();
        if (croppedFile == null) {
            showDialog("Cant create file for cropping");
            return;
        }

        final VideoProperties vp = getVideoFileProperties(_file);

        if (vp == null) {
            showDialog("Cant process video file");
            return;
        }

        int w = vp.getWidth(), h = vp.getHeight(), r = vp.getRotation();

        //switch sizes according to rotation
        switch (r) {
            case 0:
            case 180:
                //same values
                break;

            case 90:
            case 270:
                w = vp.getHeight();
                h = vp.getWidth();
                break;
        }

        if (w == h) {
            showDialog("Video already squared");
            return;
        }

        updateStatus(String.format(Locale.getDefault(), "Video prop: w=%d, h=%d, r=%d", w, h, r));
        final int side = Math.min(w, h);

        final int x = h < w ? (w - h) / 2 : 0;
        final int y = h < w ? 0 : (h - w) / 2;

        updateStatus(String.format(Locale.getDefault(), "Crop origin : x=%d, y=%d", x, y));

        final String cmd = String.format(Locale.getDefault(),
                "-i %s -filter:v crop=%d:%d:%d:%d -threads 5 -preset ultrafast -strict -2 %s",
                _file.getAbsolutePath(),
                side, side,
                x, y,
                croppedFile.getAbsolutePath());

        try {
            ffmpeg.execute(cmd.split(" "), new ExecuteBinaryResponseHandler() {

                long startTime = -1;
                long duration = -1;
                long progress = -1;

                @DebugLog
                @Override
                public void onStart() {
                    startTime = System.currentTimeMillis();
                    updateStatus("Started cropping");
                }

                @DebugLog
                @Override
                public void onFinish() {}

                @DebugLog
                @Override
                public void onSuccess(final String message) {
                    final int index = tvStatus.getText().toString().lastIndexOf("Progress: ");
                    tvStatus.setText(tvStatus.getText().toString().substring(0, index) + "Progress: 100%");
                    updateStatus("Successfully cropped");
                    final long time = System.currentTimeMillis() - startTime;
                    showDialog(String.format(Locale.getDefault(),"Processing time: %d ms\nFile: %s", time, croppedFile.getAbsolutePath()));
                }

                @DebugLog
                @Override
                public void onProgress(final String _message) {
                    if (duration == -1 && _message.contains("Duration:")) {
                        duration = parseDuration(_message);
                        updateStatus("Video duration: " + duration + " ms");
                    } else if (_message.contains("time=")) {
                        progress = parseTime(_message);
                    }

                    if (duration != -1 && progress != -1) {
                        final float percent = progress * 100 / (float) duration;
                        final String progressStr = "Progress: " + String.format(Locale.getDefault(), "%.2f", percent) + "%";
                        if (tvStatus.getText().toString().contains("Progress: ")) {
                            final int index = tvStatus.getText().toString().lastIndexOf("Progress: ");
                            tvStatus.setText(tvStatus.getText().toString().substring(0, index) + progressStr);
                        } else {
                            updateStatus(progressStr);
                        }
                    }
                }

                @DebugLog
                @Override
                public void onFailure(final String message) {
                    updateStatus("Cropping failed");
                }
            });
        } catch (final FFmpegCommandAlreadyRunningException _e) {
            _e.printStackTrace();
            showDialog("FFmpegCommandAlreadyRunningException");
        }
    }

    @DebugLog
    @Nullable
    private File prepareVideoFile() {
        if(!Environment.getExternalStorageState().equalsIgnoreCase("mounted")) {
            return null;
        } else {
            final File mediaStorageDir = new File(Environment.getExternalStorageDirectory(), "Camera");
            return !mediaStorageDir.exists() && !mediaStorageDir.mkdirs() ? null : new File(mediaStorageDir.getPath() + File.separator + System.currentTimeMillis() + "_squared.mp4");
        }
    }

    @DebugLog
    @Nullable
    private VideoProperties getVideoFileProperties(final File _file) {
        final MediaMetadataRetriever metaRetriever = new MediaMetadataRetriever();
        metaRetriever.setDataSource(_file.getAbsolutePath());
        String width = metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH);
        String height = metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT);
        String rotation = metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_ROTATION);

        int r = 90;
        try {
            r = Integer.parseInt(rotation);
        } catch (final NumberFormatException _e_) {}

        try {
            return new VideoProperties(Integer.parseInt(width), Integer.parseInt(height), r);
        } catch (final NumberFormatException _e_) {
            return null;
        }
    }

    @DebugLog
    private long parseDuration(final String _str) {
        String res = _str.trim();
        final int durIndx = res.indexOf("Duration:");
        res = res.substring(durIndx, res.length());
        res = res.replaceAll(" ", "");
        res = res.replace("Duration:", "");
        final int comma = res.indexOf(",");
        res = res.substring(0, comma);
        final SimpleDateFormat formatter = new SimpleDateFormat("hh:mm:ss.SS", Locale.getDefault());
        formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
        try {
            return formatter.parse(res).getTime();
        } catch (final ParseException _e) {
            _e.printStackTrace();
            return -1;
        }
    }

    @DebugLog
    private long parseTime(final String _str) {
        String res = _str.trim();
        final int timeIndx = res.indexOf("time=");
        res = res.substring(timeIndx, res.length());
        final int bitrateIndx = res.indexOf("bitrate=");
        res = res.substring(0, bitrateIndx);
        res = res.replace("time=", "");
        res = res.replaceAll(" ", "");
        final SimpleDateFormat formatter = new SimpleDateFormat("hh:mm:ss.SS", Locale.getDefault());
        formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
        try {
            return formatter.parse(res).getTime();
        } catch (final ParseException _e) {
            _e.printStackTrace();
            return -1;
        }
    }

}

package com.psixgame.apps.squarecamera;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.FrameLayout;

/**
 * Created by ZOG on 12/24/2017.
 */

public final class SquareFrameLayout extends FrameLayout {
    public SquareFrameLayout(@NonNull final Context context) {
        super(context);
    }

    public SquareFrameLayout(@NonNull final Context context, @Nullable final AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(final int widthMeasureSpec, final int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }
}

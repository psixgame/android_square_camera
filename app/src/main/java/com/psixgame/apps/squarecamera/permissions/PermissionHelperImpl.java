package com.psixgame.apps.squarecamera.permissions;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.PermissionInfo;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;

import com.psixgame.apps.squarecamera.BaseActivity;
import com.psixgame.apps.squarecamera.BaseFragment;

import hugo.weaving.DebugLog;

/**
 * Created by ZOG on 11/20/2017.
 */

public final class PermissionHelperImpl implements IPermissionHelper {

    private final Context mContext;

    public PermissionHelperImpl(final Context _context) {
        mContext = _context;
    }

    private String mPermission = null;
    private int mPermissionRequestCode = -1;
    private Callback mCallback = null;

    @DebugLog
    public final boolean hasPermission(@NonNull final String _permission) {
        return ContextCompat.checkSelfPermission(mContext, _permission) == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public final void requestPermission(@NonNull final BaseActivity _activity, @NonNull final String _permission, final int _code,
                                        @NonNull final Callback _callback) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) return;

        mPermission = _permission;
        mPermissionRequestCode = _code;
        mCallback = _callback;
        _activity.requestPermissions(new String[]{_permission}, _code);
    }

    @Override
    public final void requestPermission(@NonNull final BaseFragment _fragment, @NonNull final String _permission, final int _code,
                                        @NonNull final Callback _callback) {
        mPermission = _permission;
        mPermissionRequestCode = _code;
        mCallback = _callback;
        _fragment.requestPermissions(new String[]{_permission}, _code);
    }

    @DebugLog
    @Override
    public final void onRequestPermissionsResult(final int _requestCode, @NonNull final String[] _permissions, @NonNull final int[] _grantResults) {
        if (_requestCode == mPermissionRequestCode) {
            boolean result = _grantResults.length > 0 && _grantResults[0] == PackageManager.PERMISSION_GRANTED;
            mCallback.onPermissionResult(mPermission, mPermissionRequestCode, result);
        }
    }

    @DebugLog
    public final String getPermissionName(@NonNull final String _permission) {
        final PackageManager pm = mContext.getPackageManager();
        String name = _permission;
        try {
            final PermissionInfo info = pm.getPermissionInfo(_permission, PackageManager.GET_META_DATA);
            name = info.loadLabel(pm).toString();
        } catch (final PackageManager.NameNotFoundException _e) {
            _e.printStackTrace();
        }
        return name;
    }

}

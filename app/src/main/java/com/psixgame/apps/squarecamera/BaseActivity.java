package com.psixgame.apps.squarecamera;

import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import com.psixgame.apps.squarecamera.permissions.IPermissionHelper;
import com.psixgame.apps.squarecamera.permissions.PermissionHelperImpl;

import hugo.weaving.DebugLog;

/**
 * Created by ZOG on 12/19/2017.
 */

public abstract class BaseActivity extends AppCompatActivity implements IPermissionHelper.Callback {

    protected IPermissionHelper mPermissionHelper;

    @Override
    protected void onCreate(@Nullable final Bundle _savedInstanceState) {
        super.onCreate(_savedInstanceState);
        mPermissionHelper = new PermissionHelperImpl(getApplicationContext());
    }

    @Override
    public final void onRequestPermissionsResult(final int _requestCode, @NonNull final String[] _permissions, @NonNull final int[] _grantResults) {
        super.onRequestPermissionsResult(_requestCode, _permissions, _grantResults);
        mPermissionHelper.onRequestPermissionsResult(_requestCode, _permissions, _grantResults);
    }

    /**
     * Override in order to add custom behavior.
     */
    @CallSuper
    @DebugLog
    public void onPermissionResult(final String _permission, final int _code, final boolean _granted) {
        if (!_granted) showDialog("Grant all permissions");
    }

    @DebugLog
    protected void showDialog(final String _msg) {
        new AlertDialog.Builder(this)
                .setTitle("Info")
                .setMessage(_msg)
                .setPositiveButton("ok", null)
                .show();
    }
}

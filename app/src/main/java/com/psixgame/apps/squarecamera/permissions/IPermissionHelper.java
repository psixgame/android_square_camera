package com.psixgame.apps.squarecamera.permissions;

import android.support.annotation.NonNull;

import com.psixgame.apps.squarecamera.BaseActivity;
import com.psixgame.apps.squarecamera.BaseFragment;

/**
 * Helper for android permission logic.
 * Created by ZOG on 11/20/2017.
 */

public interface IPermissionHelper {
    /**
     * Check if application has permission.
     *
     * @param _permission permission string.
     * @return true if has permission, false otherwise.
     */
    boolean hasPermission(@NonNull String _permission);

    /**
     * Requests permission from user.
     *
     * @param _activity   activity requesting permission from.
     * @param _permission permission string.
     * @param _code       request code for matching result.
     */
    void requestPermission(@NonNull BaseActivity _activity, @NonNull String _permission, int _code, @NonNull Callback _callback);
    void requestPermission(@NonNull BaseFragment _fragment, @NonNull String _permission, int _code, @NonNull Callback _callback);

    /**
     * Input method from activity/fragment for handling result. Call it from {@link android.support.v4.app.Fragment#onRequestPermissionsResult(int, String[], int[])}
     * or {@link android.app.Activity#onRequestPermissionsResult(int, String[], int[])}.
     */
    void onRequestPermissionsResult(int _requestCode, @NonNull String[] _permissions, @NonNull int[] _grantResults);

    /**
     * Returns permission system label.
     *
     * @param _permission requested permission.
     */
    String getPermissionName(@NonNull String _permission);

    /**
     * Interface for notifying about operation result.
     */
    interface Callback {
        /**
         * Method for notifying about operation result.
         *
         * @param _permission requested permission string.
         * @param _code       code with which permission was requested.
         * @param _granted    true if granted, false otherwise.
         */
        void onPermissionResult(String _permission, int _code, boolean _granted);
    }

}
